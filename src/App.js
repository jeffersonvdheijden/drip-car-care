import { Provider } from 'react-redux';
import store from './helpers/state/store';
import { BrowserRouter as Router, Switch, Route, useLocation } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from "react-transition-group";

import './helpers/styles/grid.scss';
import './helpers/styles/colors.scss';
import './helpers/styles/fonts.scss';
import './helpers/styles/animations.scss';

import DotRing from './modules/Shared/customCursor/DotRing';

import Navigation from './modules/Shared/Navigation/Navigation';
import Homepage from './modules/Homepage/components/Homepage/Homepage';

export default function App() {
  return (
    <Provider store={store}>
      <Router>
        <Navigation />
        <DotRing />
        <Switch>
          <Route path="*">
            <AnimatedSwitch />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
}

function AnimatedSwitch() {
  let location = useLocation(); 

  return (
    <TransitionGroup>
      <CSSTransition
        key={location.key}
        classNames={'fade'}
        timeout={1800}
      >
        <div className={'pageTransition'}>
          <div className={'pageContent'}>
            <Switch location={location}>
              <Route exact path="/">
                <Homepage />
              </Route>
              <Route path="/portfolio">
                Portfolio
              </Route>
              <Route path="/about">
                <div style={{ paddingTop: '100px', background: 'red' }}>About</div>
              </Route>
              <Route path="/contact">
                Contact
              </Route>
            </Switch>
          </div>
          <div className={'inAnimation'}></div>  
        </div>
      </CSSTransition>
    </TransitionGroup>
  )
}