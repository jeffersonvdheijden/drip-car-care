import { combineReducers } from 'redux';

import homepage from './../../modules/Homepage/reducers';

const rootReducer = combineReducers({
    homepage
});

export default rootReducer;