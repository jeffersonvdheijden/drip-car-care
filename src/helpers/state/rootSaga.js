import { all } from 'redux-saga/effects';

import homepage from './../../modules/Homepage/watchers';

export function* rootSaga() {
    const combinedWatchers = [
        homepage
    ];
    yield all(combinedWatchers.map(v => v()));
}
