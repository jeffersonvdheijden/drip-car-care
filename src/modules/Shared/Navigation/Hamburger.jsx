import React from 'react';

const Hamburger = () => {
    return (
        <svg width="42" height="18" viewBox="0 0 42 18" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect y="14" width="42" height="4" rx="2" fill="#1F2233"/>
            <rect y="7" width="42" height="4" rx="2" fill="#1F2233"/>
            <rect width="42" height="4" rx="2" fill="#1F2233"/>
        </svg>
    )
}

export default Hamburger;
