import React from 'react';
import Modal from 'react-modal';
import { CSSTransition } from 'react-transition-group';
import * as styles from './AppointmentModal.module.scss';

const AppointmentModal = ({
    modalIsOpen,
    closeModal
}) => {
    return (
        <>
            <CSSTransition
                in={modalIsOpen}
                timeout={300}
                classNames="dialog"
            >
                <Modal
                    isOpen={modalIsOpen}
                    onRequestClose={closeModal}
                    contentLabel="Make an appointment"
                    closeTimeoutMS={500}
                    className={styles.modal}
                    overlayClassName={styles.overlay}
                >
                    Please leave us your name and phone number. We'll contact you as soon as possible!
                    <form className={styles.appointmentForm}>
                        <input type={'text'} placeholder={'Name'} />
                        <input type={'text'} placeholder={'Phone number'} />
                        <button 
                            className={styles.cancel}                            
                            onClick={(e) => { e.preventDefault(); closeModal(true); }}
                        >
                            Cancel
                        </button>
                        <button 
                            className={styles.send}
                            onClick={(e) => { e.preventDefault(); }}
                        >
                            Send
                        </button>
                    </form>
                </Modal>
            </CSSTransition>
        </>
    )
}

export default AppointmentModal;