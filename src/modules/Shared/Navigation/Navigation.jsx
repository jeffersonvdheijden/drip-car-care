import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import * as styles from './Navigation.module.scss';
import { MouseContext } from './../../../helpers/context/mouse-context';
import AppointmentModal from './AppointmentModal';
// import Hamburger from './Hamburger';

const Navigation = () => {
    const [showModal, setShowModal] = useState(false);
    const { cursorChangeHandler } = useContext(MouseContext);
    return (
        <>
            <div className={styles.navigation}>
                <div className={'container'}>
                    <div className="col-12">
                        <div className={styles.navigationInner}>
                            <ul>
                                <li>
                                    <Link 
                                        onMouseEnter={() => cursorChangeHandler("hovered")}
                                        onMouseLeave={() => cursorChangeHandler("")} 
                                        to="/"
                                    >
                                        Home
                                    </Link>
                                </li>
                                <li>
                                    <Link 
                                        onMouseEnter={() => cursorChangeHandler("hovered")}
                                        onMouseLeave={() => cursorChangeHandler("")} 
                                        to="/about"
                                    >
                                        About
                                    </Link>
                                </li>
                                <li>
                                    <Link 
                                        onMouseEnter={() => cursorChangeHandler("hovered")}
                                        onMouseLeave={() => cursorChangeHandler("")} 
                                        to="/services"
                                    >
                                        Services
                                    </Link>
                                </li>
                                <li>
                                    <Link 
                                        onMouseEnter={() => cursorChangeHandler("hovered")}
                                        onMouseLeave={() => cursorChangeHandler("")} 
                                        to="/portfolio"
                                    >
                                        Portfolio
                                    </Link>
                                </li>
                                <li>
                                    <Link 
                                        onMouseEnter={() => cursorChangeHandler("hovered")}
                                        onMouseLeave={() => cursorChangeHandler("")} 
                                        to="/contact"
                                    >
                                        Contact
                                    </Link>
                                </li>
                                <li>
                                    <button 
                                        onMouseEnter={() => cursorChangeHandler("hovered")}
                                        onMouseLeave={() => cursorChangeHandler("")} 
                                        onClick={() => setShowModal(true)}
                                    >
                                        Make an appointment
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <AppointmentModal 
                modalIsOpen={showModal}
                closeModal={ () => { setShowModal(false) } }
            />
        </>
    )
}

export default Navigation;