import React, { useRef } from 'react';
import { useInViewport } from 'react-in-viewport';
import classNames from 'classnames';
import * as styles from './Ecofriendly.module.scss';

const Ecofriendly = () => {
    const ecoRef = useRef();
    const { inViewport, enterCount } = useInViewport(
        ecoRef, 
        { rootMargin: '-400px' }, 
        { disconnectOnLeave: false }, 
        {}
    );

    const ecoCN = classNames({
        [styles.ecofriendly]: true,
        [styles.animate]: (enterCount === 1 && inViewport),
        [styles.done]: (enterCount >= 1)
    });

    return (
        <div className={'container'}>
            <div className={'col-12'}>
                <div className={ecoCN} ref={ecoRef}>
                    <div className={styles.video}>
                        <video
                            className={styles.videoPlayer} 
                            autoPlay 
                            muted
                            loop
                        >
                            <source src="/video/ocean.mp4" type="video/mp4" />
                            Your browser does not support the video tag.
                        </video>
                    </div>
                    <div className={styles.text}>
                        <h2 className={'large'}>The Earth is what we all have in common</h2>
                        <p>That's why we believe it is vital we reduce the number of times your car needs to be washed, as well as the amount of water and chemicals used.</p>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Ecofriendly;