import React, { useState, useCallback, useContext, useRef } from 'react';
import * as styles from './Portfolio.module.scss';
import { MouseContext } from './../../../../helpers/context/mouse-context';
import { photos } from './Photos';
import classNames from 'classnames';
import { useInViewport } from 'react-in-viewport';
import Gallery from 'react-photo-gallery';
import Carousel, { Modal, ModalGateway } from "react-images";

const Portfolio = () => {
    const [currentImage, setCurrentImage] = useState(0);
    const [viewerIsOpen, setViewerIsOpen] = useState(false);
  
    const openLightbox = useCallback((event, { photo, index }) => {
      setCurrentImage(index);
      setViewerIsOpen(true);
    }, []);
  
    const closeLightbox = () => {
      setCurrentImage(0);
      setViewerIsOpen(false);
    };

    const { cursorChangeHandler } = useContext(MouseContext);

    const portfolioRef = useRef();
    const { inViewport, enterCount } = useInViewport(
        portfolioRef, 
        { rootMargin: '-200px' }, 
        { disconnectOnLeave: false }, 
        {}
    );

    const portfolioCN = classNames({
        [styles.portfolio]: true,
        [styles.animate]: (enterCount === 1 && inViewport),
        [styles.done]: (enterCount >= 1)
    });

    return (
        <div className={portfolioCN} ref={portfolioRef}>
            <div className={'container'}>
                <h2>
                    Portfolio
                </h2>
            </div>
            <div
                onMouseOver={() => cursorChangeHandler("view") }
                onMouseLeave={() => cursorChangeHandler("")} 
            >
                <Gallery 
                    margin={7.5}
                    photos={photos} 
                    onClick={openLightbox} 
                />
            </div>
            <ModalGateway>
                {viewerIsOpen ? (
                    <Modal onClose={closeLightbox}>
                        <Carousel
                            currentIndex={currentImage}
                            views={photos.map(x => ({
                                ...x,
                                srcset: x.srcSet,
                                caption: x.title
                            }))}
                        />
                    </Modal>
                ) : null}
            </ModalGateway>
        </div>
    )
}

export default Portfolio;