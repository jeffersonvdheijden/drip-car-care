export const photos = [
    {
        src: "/images/portfolio/1.jpg",
        width: 3,
        height: 2
    },
    {
        src: "/images/portfolio/2.jpg",
        width: 2,
        height: 2
    },
    {
        src: "/images/portfolio/3.jpg",
        width: 3,
        height: 2
    },
    {
        src: "/images/portfolio/4.jpg",
        width: 1,
        height: 2
    },
    {
        src: "/images/portfolio/5.jpg",
        width: 3,
        height: 2
    },
    {
        src: "/images/portfolio/6.jpg",
        width: 3,
        height: 2
    },
    {
        src: "/images/portfolio/7.jpg",
        width: 1,
        height: 2
    },
    {
        src: "/images/portfolio/8.jpg",
        width: 3,
        height: 2
    },
    {
        src: "/images/portfolio/9.jpg",
        width: 3,
        height: 2
    },
    {
        src: "/images/portfolio/10.jpg",
        width: 2,
        height: 2
    },
    {
        src: "/images/portfolio/11.jpg",
        width: 3,
        height: 2
    }
];
