import React, { useContext } from 'react';
import * as styles from './Appointment.module.scss';
import { MouseContext } from './../../../../helpers/context/mouse-context';

const Appointment = () => {
    const { cursorChangeHandler } = useContext(MouseContext);
    
    return (
        <div 
            className={styles.appointment}
                onMouseEnter={() => cursorChangeHandler("white") }
                onMouseLeave={() => cursorChangeHandler("")}  
        > 
            <div className={'container'}>
                Appointment 
            </div>
        </div>
    )
}

export default Appointment;