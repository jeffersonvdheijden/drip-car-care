import React from 'react';
import Header from './../Header/Header';
import Services from './../Services/Services';
import Reviews from './../Reviews/Reviews';
import Ecofriendly from './../Ecofriendly/Ecofriendly';
import Portfolio from './../Portfolio/Portfolio';
import Appointment from './../Appointment/Appointment';

const Homepage = () => {
    return (
        <div className={'homepage'}>
            <Header />
            <Services />
            <Reviews />
            <Ecofriendly />
            <Portfolio />
            <Appointment />
        </div>
    )
}

export default Homepage;