import React, { useContext, useState, useRef } from 'react';
import classNames from 'classnames';
import * as styles from './Header.module.scss';
import { useInViewport } from 'react-in-viewport';
import { MouseContext } from './../../../../helpers/context/mouse-context';

const Header = () => {
    const { cursorChangeHandler } = useContext(MouseContext);
    const [fullScreen, setFullScreen] = useState(false);
    const videoRef = useRef();
    const headerRef = useRef();
    const { inViewport, enterCount } = useInViewport(
        headerRef, 
        { rootMargin: '-200px' }, 
        { disconnectOnLeave: false }, 
        {}
    );

    const headerClassNames = classNames({
        [styles.header]: true,
        [styles.animate]: (enterCount === 1 && inViewport),
        [styles.done]: enterCount >= 1,
        [styles.fullScreen]: fullScreen
    });

    const videoClick = () => {
        !fullScreen ? cursorChangeHandler("close") : cursorChangeHandler("video");         
        if (!fullScreen) {
            videoRef.current.pause();
            videoRef.current.currentTime = 0;
            videoRef.current.play();
        }
        if (!videoRef.current.fullscreen) {
            videoRef.current.volume = 0.5;
            videoRef.current.muted = !videoRef.current.muted;
            setFullScreen(!fullScreen);
        }
    }

    const fullScreenClick = () => {
        videoRef.current.requestFullscreen();
    }

    return (        
        <div className={'container'}>
            <div className={'col-12'}>
                <div className={headerClassNames} ref={headerRef}>
                    <div className={styles.video}>
                        <div 
                            className={styles.fullscreenRequest}
                            onClick={() => fullScreenClick()}
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" width="24" height="24" viewBox="0 0 24 24"><path d="M24 9h-2v-5h-7v-2h9v7zm-9 13v-2h7v-5h2v7h-9zm-15-7h2v5h7v2h-9v-7zm9-13v2h-7v5h-2v-7h9zm11 4h-16v12h16v-12z"/></svg>
                        </div>
                        <video
                            ref={videoRef}
                            onMouseMove={() => { fullScreen ? cursorChangeHandler("close") : cursorChangeHandler("video") }}
                            onMouseLeave={() => cursorChangeHandler("")}  
                            onClick={() => videoClick()}
                            className={styles.videoPlayer} 
                            autoPlay 
                            muted
                            loop
                        >
                            <source src="/video/header.mp4" type="video/mp4" />
                            Your browser does not support the video tag.
                        </video>
                    </div>
                    <div className={styles.text}>
                        <h1 className={'large'}>DRIP CAR CARE</h1>
                        <h2>The future of auto detailing</h2>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header;