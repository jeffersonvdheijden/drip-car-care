import React, { useRef, useContext } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import classNames from 'classnames';
// import SwiperCore, { Pagination } from 'swiper/core';
import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';
import { useInViewport } from 'react-in-viewport';
import { MouseContext } from './../../../../helpers/context/mouse-context';
import * as styles from './Services.module.scss';

const Services = () => {
    const servicesRef = useRef();
    const { inViewport, enterCount } = useInViewport(
        servicesRef, 
        { rootMargin: '-200px' }, 
        { disconnectOnLeave: false }, 
        {}
    );

    // SwiperCore.use([Pagination]);
    const { cursorChangeHandler } = useContext(MouseContext);

    const slideCN = classNames({
        [styles.serviceSlide]: true,
        [styles.animate]: (enterCount === 1 && inViewport),
        [styles.done]: (enterCount >= 1)
    });

    const headerCN = classNames({
        [styles.headerText]: true,
        [styles.animate]: (enterCount === 1 && inViewport),
        [styles.done]: (enterCount >= 1)
    });

    return (
        <div className={styles.services}>
            <div className={'container'}>
                <h2 className={headerCN}>
                    Services
                </h2>
            </div>
            <div 
                className={styles.servicesSlider}
                ref={servicesRef}
            >
                <div 
                    className={'container'}
                    onMouseEnter={() => cursorChangeHandler("slider") }
                    onMouseLeave={() => cursorChangeHandler("")}  
                >
                    <Swiper
                        spaceBetween={50}
                        slidesPerView={3}
                        // pagination={{'clickable': true}}
                    >
                        <SwiperSlide className={slideCN}>
                            <img src={'/images/services/exterior.png'} alt={'Exterior'} />
                            <h3>Exterior</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt consequatur est perspiciatis vitae cum ea reprehenderit excepturi porro fuga quam possimus delectus corporis officiis eaque, repudiandae, non, doloremque omnis accusantium.
                            </p>
                            <button
                                onMouseEnter={() => cursorChangeHandler("hovered") }
                                onMouseLeave={() => cursorChangeHandler("")}  
                            >
                                Read more
                            </button>
                        </SwiperSlide>
                        <SwiperSlide className={slideCN}>
                            <img src={'/images/services/interior.png'} alt={'Exterior'} />
                            <h3>Interior</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt consequatur est perspiciatis vitae cum ea reprehenderit excepturi porro fuga quam possimus delectus corporis officiis eaque, repudiandae, non, doloremque omnis accusantium.
                            </p>
                            <button
                                onMouseEnter={() => cursorChangeHandler("hovered") }
                                onMouseLeave={() => cursorChangeHandler("")}  
                            >
                                Read more
                            </button>
                        </SwiperSlide>
                        <SwiperSlide className={slideCN}>
                            <img src={'/images/services/polishing.png'} alt={'Exterior'} />
                            <h3>Polishing</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt consequatur est perspiciatis vitae cum ea reprehenderit excepturi porro fuga quam possimus delectus corporis officiis eaque, repudiandae, non, doloremque omnis accusantium.
                            </p>
                            <button
                                onMouseEnter={() => cursorChangeHandler("hovered") }
                                onMouseLeave={() => cursorChangeHandler("")}  
                            >
                                Read more
                            </button>
                        </SwiperSlide>
                        <SwiperSlide className={slideCN}>
                            <img src={'/images/services/ceramic-coating.png'} alt={'Exterior'} />
                            <h3>Ceramic coating</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt consequatur est perspiciatis vitae cum ea reprehenderit excepturi porro fuga quam possimus delectus corporis officiis eaque, repudiandae, non, doloremque omnis accusantium.
                            </p>
                            <button
                                onMouseEnter={() => cursorChangeHandler("hovered") }
                                onMouseLeave={() => cursorChangeHandler("")}  
                            >
                                Read more
                            </button>
                        </SwiperSlide>
                        <SwiperSlide className={slideCN}>                            
                            <img src={'/images/services/other-detailing.png'} alt={'Exterior'} />
                            <h3>Other detailing</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt consequatur est perspiciatis vitae cum ea reprehenderit excepturi porro fuga quam possimus delectus corporis officiis eaque, repudiandae, non, doloremque omnis accusantium.
                            </p>
                            <button
                                onMouseEnter={() => cursorChangeHandler("hovereded") }
                                onMouseLeave={() => cursorChangeHandler("")}  
                            >
                                Read more
                            </button>
                        </SwiperSlide>
                    </Swiper>
                </div>
            </div>
        </div>
    )
}

export default Services;